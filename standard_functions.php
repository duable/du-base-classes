<?php
/*
Name: Duable Trust Framework
URI: http://duable.com
Description: Add extra functionality and options to WP.
Version: 1.0
Author: Duable Brand Trust
Author URI: http://duable.com
License: GPL2
*/

/**
 * Return the title/primary headline of the page
 * @param  string $post
 * @return string
 */
function du_page_title( $id = null ) {
  global $post;
  if ( is_search() )
    return 'Search Results';
  if ( empty( $id ) && !empty( $post->ID ) ) :
    $id = $post->ID;
  endif;
  if ( is_404() )
    return '404 Page Not Found';
  if ( is_home() )
    $id = get_option( 'page_for_posts' );
  if ( empty( $id ) ) return false;
  $main_headline = get_the_title( $id );
  return ( !empty( $main_headline ) ? $main_headline : false );
}

/**
 * Return the description of the page. Degrades to excerpt if description not available.
 * @param  string $post
 * @return string
 */
function du_page_description( $id = null ) {
  global $post;

  if ( !empty( $post->ID ) ) :
    $page_description = get_post_meta( $post->ID, 'du_page_description', true );
    $stripped_excerpt = strip_tags( get_the_excerpt() );
    return ( ( $page_description ) ? $page_description : $stripped_excerpt );
  else :
    return false;
  endif;
}


function du_trim_text( $text, $maxchar, $end = '...' ) {
  if ( strlen( $text ) > $maxchar || $text == '') {
      $words = preg_split('/\s/', $text);      
      $output = '';
      $i      = 0;
      while (1) {
          $length = strlen($output)+strlen($words[$i]);
          if ($length > $maxchar) {
              break;
          } 
          else {
              $output .= " " . $words[$i];
              ++$i;
          }
      }
      $output .= $end;
  } 
  else {
      $output = $text;
  }
  return $output;
}


/**
 * Get all post types
 * 
 * @param  [type] $post_type_args [description]
 * @return [type]                 [description]
 */
function du_get_post_types( $post_type_args = null ) {

  $args = array( 'public' => true, 'publicly_queryable' => true );

  if ( is_array( $post_type_args ) && !empty( $post_type_args ) ) :
    foreach ( $post_type_args as $key => $arg ) 
      $args[ $key ] = $arg;
  endif;

  $post_types = array();

  $post_type_query = get_post_types( $args, 'objects' );

  $post_types[ 'any' ] = 'Any';

  foreach ( $post_type_query as $key => $post_type ) :
    $post_types[ $post_type->name ] = $post_type->label;
  endforeach;

  return $post_types;
}


/**
 * Get all taxonomies
 * 
 * @param  [type] $taxonomy_args [description]
 * @return [type]                [description]
 */
function du_get_taxonomies( $taxonomy_args = null ) {

  $args = array( 'public' => true );

  if ( is_array( $taxonomy_args ) && !empty( $taxonomy_args ) ) :
    foreach ( $taxonomy_args as $key => $arg ) 
      $args[ $key ] = $arg;
  endif;

  $taxonomies = array();

  $taxonomy_query = get_taxonomies( $args, 'objects' );

  foreach ( $taxonomy_query as $key => $taxonomy ) :
    $taxonomies[ $taxonomy->name ] = $taxonomy->label;
  endforeach;

  return $taxonomies;
}


/**
 * Get all terms from taxonomy
 * 
 * @param  [type] $taxonomy_args [description]
 * @return [type]                [description]
 */
function du_get_terms( $taxonomy_args = null ) {
  $taxonomies = du_get_taxonomies( $taxonomy_args );
  $terms      = array();
  foreach ( $taxonomies as $tax_slug => $tax_label ) :
    $terms_query = get_terms( array(
        'taxonomy'    => $tax_slug,
        'hide_empty'  => false,
    ));
    foreach ( $terms_query as $term ) :
      $terms[ $term->term_id ] = $tax_label . "/" . $term->name;
    endforeach;
  endforeach;
  return $terms;
}


/**
 * Get templates
 * 
 * @param  [type] $template_type [description]
 * @param  [type] $path          [description]
 * @return [type]                [description]
 */
function du_get_templates( $template_type = null, $path = null ) {
  if ( empty( $template_type ) )
    return;

  # House all templates to return
  $templates  = array();

  # Grab all files from template directory
  $path           = get_stylesheet_directory() . '/templates/' . $template_type;
  $override_path  = get_stylesheet_directory() . '/vendor/duable/du-elementor/templates/' . $template_type;
  
  if ( file_exists( $path ) ) :
    $files      = array_diff( scandir( $path ), array( '.', '..' ) );
  else :
    $files = array();
  endif;

  if ( file_exists( $override_path ) ) :
    $override_files = array_diff( scandir( $override_path ), array( '.', '..' ) );
  else :
    $override_files = array();
  endif;
 
  $files = array_merge( $files, $override_files );
  ksort( $files );

  # Add all template file names and labels to $templates array
  foreach ( $files as $index => $file ) :
    $file = sanitize_title( str_replace( '.php', '', $file ) );
    $name = ucwords( str_replace( '_', ' ', str_replace('-', ' ', $file ) ) );
    $templates[ $file ] = $name;
  endforeach;

  return $templates;
}


/**
 * Return path to specified asset
 * @param  str $url path to asset
 * @return str
 */
function du_site_asset( $url, $meta_key = null ) {
  /**
   * Taken from get_home_path() [wp-admin/includes/file.php]
   */
  $home = get_option( 'home' );
  $siteurl = get_option( 'siteurl' );
  if ( $home != '' && $home != $siteurl ) {
    $wp_path_rel_to_home = str_replace($home, '', $siteurl); /* $siteurl - $home */
    $pos = strpos($_SERVER["SCRIPT_FILENAME"], $wp_path_rel_to_home);
    $home_path = substr($_SERVER["SCRIPT_FILENAME"], 0, $pos);
    $home_path = trailingslashit( $home_path );
  } else {
    $home_path = ABSPATH;
  }

  /**
   * Get theme object to pull slug
   * @author  mohammad <mohammad@duable.com>
   */
  $theme_info   = wp_get_theme();
  $file_path    = ( file_exists( $home_path . '/global' . $url ) ?  '/global' . $url : content_url()  . '/themes/' . $theme_info->stylesheet . $url );


  return $file_path;
}

function du_is_live_site() {
  /**
   * First, we will get contents of .du_push file
   */
  $du_push_path = get_stylesheet_directory() . '/.du_push';
  
  if ( file_exists( $du_push_path ) ) :
    $du_push_contents = file_get_contents( $du_push_path, FILE_USE_INCLUDE_PATH );
    
    /**
     * We will now get the last variable section for LIVE url
     */
    $du_push_contents = explode( ',', $du_push_contents );
    $live_host = $du_push_contents[3];
    
    /**
     * Still don't have the exact URL.  We need to get the variable
     * within single quotes
     */
    $live_host = explode( "'", $live_host );
    
    /**
     * $live_host is now the LIVE url declared in .du_push
     */
    $live_host = $live_host[1];
    
    /**
     * If we are on the live host, then return true.  Otherwise, false.
     */
    return ( ( $live_host == $_SERVER[ 'HTTP_HOST' ] ) || ( $_SERVER[ 'HTTP_HOST' ] == 'www.' . $live_host ) ? true : false );
    
  else :

    /**
     * .du_push file doesn't exist, so let's just return true.
     */
    return true;

  endif;
}

function du_youtube_embed_link( $url, $height, $width ){
  parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars ); 
  echo '<iframe width="' . $width . '" height="' . $height . '" src="//www.youtube.com/embed/' . $my_array_of_vars['v'] . '" frameborder="0" allowfullscreen></iframe>';
}

function dimox_breadcrumbs() {
  $delimiter = '&raquo;';
  $home = 'Home'; // text for the 'Home' link
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb

  if ( !is_home() && !is_front_page() || is_paged() ) {

    echo '<div id="crumbs">';

    global $post;
    $homeLink = get_bloginfo('url');
    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $before . '' . single_cat_title('', false) . '' . $after;

    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo $before . get_the_title() . $after;
      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); 
      if ( isset($cat[0]) ) {
        $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      }
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;

    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;

    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;

    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
/*
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 */
    echo '</div>';

  }
}

/**
 * Get the current page template name.
 * Note that a page template is different from a Wordpress theme!
 * @return string
 */
function du_page_template_name() {
  if ( is_page() ) {  // Currently displaying a Page?
    global $post;
    return str_replace( '.php', '', get_post_meta( $post->ID, '_wp_page_template', true ) );
  }
  return false;
}

/**
 * Pagination Script
 * Taken from twentyfourteen theme
 */
function du_paging_nav( $the_query='wp_query' ) {
  /**
   * Don't print empty markup if there's only one page.
   */
  if ( $GLOBALS[$the_query]->max_num_pages < 2 ) {
    return;
  }

  $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
  $pagenum_link = html_entity_decode( get_pagenum_link() );
  $query_args   = array();
  $url_parts    = explode( '?', $pagenum_link );

  if ( isset( $url_parts[1] ) ) {
    wp_parse_str( $url_parts[1], $query_args );
  }

  $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
  $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

  $format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
  $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

  // Set up paginated links.
  $links = paginate_links( array(
    'base'     => $pagenum_link,
    'format'   => $format,
    'total'    => $GLOBALS['wp_query']->max_num_pages,
    'current'  => $paged,
    'mid_size' => 1,
    'add_args' => array_map( 'urlencode', $query_args ),
    'prev_text' => __( '&larr; Previous', 'twentyfourteen' ),
    'next_text' => __( 'Next &rarr;', 'twentyfourteen' ),
  ) );

  if ( $links ) :

  ?>
  <nav class="navigation paging-navigation" role="navigation">
    <div class="pagination loop-pagination">
      <?php echo $links; ?>
    </div><!-- .pagination -->
  </nav><!-- .navigation -->
  <?php
  endif;
}

/**
 * Check if is a subpage
 * @return boolean 
 */
function du_is_subpage(){
  global $post;
  return ( ( is_page() && $post->post_parent ) ? $post->post_parent : false );
}

/**
 * Check if parent page
 * @return bool
 */
function du_is_parent(){
  global $post;
  $my_wp_query    = new WP_Query();
  $all_wp_pages   = $my_wp_query->query( array( 'post_type' => 'page' ) );
  $page_children  = get_page_children( $post->ID, $all_wp_pages );
  wp_rest_postdata();

  if ( $page_children ) {
      return true;
  } else {
      return false;
  }
}

/**
 * DU Options Panels
 */
class du 
{
  public static $class = __CLASS__ ;
  public function __construct( $prefix )
  {
    $this->prefix = $prefix ;
  }
  public static function __callStatic( $name , $arguments )
  {
    if ( is_subclass_of( $prefix = 'du_' . $name , 'du_options' ) )
      return new self( $prefix ) ;
  }
  public function __get( $name )
  {
    return get_option( $this->prefix . '_' . $name ) ;
  }
  public function __toString()
  {
    return print_r( $this , true ) ;
  }
}

abstract class du_options
{
  public $description = "" ;

  public $title ;

  protected $fields = array() ;

  protected $types = array(
    'default' => '<input type="text" %3$s id="%1$s" name="%1$s" value="%2$s"/>' ,
    'checkbox' => '<input type="checkbox" %3$s id="%1$s" name="%1$s" value="true"/>',
    'textblock' => '<textarea %3$s id="%1$s" name="%1$s">%2$s</textarea>',
    'heading' => '<h3 class="title" id="%1$s">%2$s</h3>',
  ) ;

  public function __construct()
  {
    add_action( 'admin_menu' , array( &$this, 'admin_menu' ) );
    add_filter( 'admin_init' , array( &$this, 'register' ) );
  }

  public function __call( $name , $args )
  {
    if ( preg_match( '/^render_/' , $name ) ) {
      $set = explode( '_' , $name ) ;
      array_shift( $set ) ;
      $index = array_shift( $set ) ;
      $field = (object) $this->fields[ $index ] ;
      @$option = implode( '_' , $name ) ;
      $value = get_option(  get_class( $this ) . '_' . $this->slugify( $field->title ) ) ;
      $class = !isset( $field->class ) ? '' : ' class="' . (is_array($field->class)?implode( ' ' , $field->class ):$field->class) . '" ' ;
      $checked = ( isset( $field->type ) && $field->type == 'checkbox' && $value ) ? ' checked="checked"' : '' ;
      echo vsprintf(
        ( isset( $field->type ) && isset( $this->types[ $field->type ] ) ) ? $this->types[ $field->type ] : $this->types['default'] ,
        array(
          get_class( $this ) . '_' . $this->slugify( $field->title ) ,
          $value ,
          $class . $checked
         )
      );
    } else return '' ;
  }

  public function admin_menu()
  {
    add_options_page(
      isset($this->title) ? $this->title : $this->titlize( get_class( $this ) ) ,
      isset($this->title) ? $this->title : $this->titlize( get_class( $this ) ) ,
      'manage_options' ,
      get_class( $this ) ,
      array( $this, 'page' )
    );
  }

  public function register()
  {
    add_settings_section(
      get_class( $this ) ,
      '<br />' . isset($this->title) ? $this->title : $this->titlize( get_class( $this ) ) ,
      array( &$this , 'description' ),
      get_class( $this )
    );
    foreach ($this->fields as $i => $field) {
      add_settings_field(
        $slug = get_class( $this ) . '_' . $this->slugify( $field['title'] ) ,
        $field['title'] ,
        array( &$this , 'render_' . $i . '_' . $this->slugify( $field['title'] ) ) ,
        get_class( $this ) ,
        get_class( $this ) ,
        array()
      ) ;
      register_setting(
        __CLASS__ . '_' . sanitize_title( $this->titlize( get_class( $this ) ) ) ,
        $slug
      ) ;
    }
  }

  public function page() {?>
  <style>
  .text-block{
    width: 300px;
    height: 250px;
  }
  </style>
    <div id="wpbody-content">
      <div class="wrap">
        <div class="icon32" id="icon-tools"><br /></div>
        <h2><?php echo isset($this->title) ? $this->title : $this->titlize( get_class( $this ) ) ; ?></h2>
        <p><?php echo $this->description; ?></p>
        <form method="post" action="options.php" enctype="multipart/form-data">
          <?php settings_fields(__CLASS__ . '_' . sanitize_title( $this->titlize( get_class( $this ) ) )); ?>
          <?php do_settings_sections(get_class( $this )); ?>
          <br /><br />
          <p class="submit">
            <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
          </p>
        </form>
      </div>
    </div>
    <?php
  }

  public function titlize($str)
  {
    return ucwords( str_replace( '_' , ' ' , preg_replace( '/^du_/' , '' , $str ) ) ) ;
  }

  public function slugify($str)
  {
    return str_replace( '-' , '_' , sanitize_title( $this->titlize( $str ) ) ) ;
  }

  public function description()
  {
    return $this->description ;
  }

}


class du_developer extends du_options
{
  public $description = "Modify at your own risk." ;

  protected $fields = array(
    array( 'title' => 'Google Analytics' , 'class' => array( 'text-block' ), 'type' => 'textblock' ) , /* textarea */
    array( 'title' => 'Google Webmaster Meta' , 'class' => array( 'regular-text' ) ) ,
  );

} new du_developer ;