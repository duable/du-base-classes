# README #

Base classes for Duable theme functionality.

## Duable Embedded Videos ##

This is used to embed a video on the page.  Place this code inside a container:

```
#!html
<div class="container">
  <div data-du-video>
    <?php du_bg_video( 'youtube_link' ); ?>
  </div>
</div>

```