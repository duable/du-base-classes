<?php
/*
*
* Add front-end code to the header and footer of the site
*
*/

class du_header_footer{

	public function __construct() {
		add_action( 'wp_head', array( $this, 'gwebmaster_code' ) );
		add_action( 'wp_head', array( $this, 'place_header_html5_shiv' ) );
		add_action( 'wp_footer', array( $this, 'place_footer_google_analytics' ) );
	}
	
	static function gwebmaster_code() { 
		if ( du::developer()->google_webmaster_meta ) {
			/**
			 * Google Site Verification
			 */
			echo 	"\n"
			. '<meta name="google-site-verification" content="' 
			. du::developer()->google_webmaster_meta 
			. '" />'
			.	"\n";
		}
	}
	
	static function place_header_html5_shiv() {
		/**
		 * HTML5 Shiv
		 */
		echo '<!--[if lt IE 9]>' . "\n"
		.		'<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>' . "\n"
		.		'<![endif]-->'
		.		"\n";
	}

	static function place_footer_google_analytics() { 
		if ( is_super_admin() ) return false;
		if ( du::developer()->google_analytics ) :
			$analytics_code = du::developer()->google_analytics;
		  echo ( du_is_live_site() ? "\n<!-- Google Analytics -->\n" . $analytics_code . "\n" : '' );
		endif;
	}

} new du_header_footer;
