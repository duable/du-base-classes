<?php
/**
 * Log errors to WP DEBUG file
 * http://www.stumiller.me/sending-output-to-the-wordpress-debug-log/
 */
if (!function_exists( 'du_write_log' )) {
  function du_write_log ( $log )  {
    if ( true === WP_DEBUG ) {
      if ( is_array( $log ) || is_object( $log ) ) {
        error_log( print_r( $log, true ) );
      } else {
        error_log( $log );
      }
    }
  }
}