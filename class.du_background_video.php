<?php 

function du_bg_video( $link=null, $options=null ) {
  /**
   * No link entered, so do nothing!
   */
  if ( empty( $link ) ) return;

  /**
   * Link is set, so let's move forward.
   */
  $video_bg = new du_background_video( $link, $options );
}

class du_background_video {
  
  public function __construct( $video_link=null, $options=null ) {

    /**
     * Grab options if they exist
     */

    if ( !empty( $options ) ) :
      /* If the options are a query string, convert to an array */
      if ( !is_array( $options ) ) parse_str( $options, $options );

      /* Let's create a private variable out of each option */
      foreach ( $options as $option => $value ) {
        $this->options[ $option ] = $value;
      }
    endif;

    /**
     * Decipher service from link.  Then grab video id and
     * output appropriate embed code.
     */
    
    /* Parsing the URL */
    $this->video_link = parse_url( $video_link );

    /* Remove WWW from hostname */
    $this->video_link[ 'host' ] = preg_replace('/^www\./i', '', $this->video_link[ 'host' ] );

    switch ( $this->video_link[ 'host' ] ) {
      case 'youtube.com' :
        /**
         * Let's grab the values from query string
         */
        parse_str( $this->video_link[ 'query' ], $this->query_vars );
        $this->video_id = $this->query_vars[ 'v' ];
        /**
         * Let's output the html
         */
        $this->output_youtube();
        break;
      
      case 'youtu.be' :
        /**
         * Let's remove the first character from the path
         * (which is a '/' when dealing with youtu.be links)
         * in order to get the video id
         */
        $this->video_id = substr( $this->video_link[ 'path' ] , 1 );
        /**
         * Let's output the html
         */
        $this->output_youtube();
        break;

      default :
        # code...
        break;
    }
    
  }

  public function output_youtube() {

    /* Check for specific options and output defaults if not set */
    $this->autoplay = ( !isset( $this->options[ 'autoplay' ] ) ? '1' : $this->options[ 'autoplay' ] );
    $this->autohide = ( !isset( $this->options[ 'autohide' ] ) ? '1' : $this->options[ 'autohide' ] );
    $this->controls = ( !isset( $this->options[ 'controls' ] ) ? '1' : $this->options[ 'controls' ] );
    $this->modestbranding = ( !isset( $this->options[ 'modestbranding' ] ) ? '1' : $this->options[ 'modestbranding' ] );
    $this->loop = ( !isset( $this->options[ 'loop' ] ) ? '1' : $this->options[ 'loop' ] );
    $this->mute = ( !isset( $this->options[ 'mute' ] ) ? '1' : $this->options[ 'mute' ] );
    $this->playlist = ( $this->loop == 1 ? 'playlist=' . $this->video_id . '&' : '' );

    /**
     * Output the youtube iframe html
     */
    echo '<iframe data-src="//www.youtube.com/embed/' 
      . $this->video_id . '?' 
      . $this->playlist 
      . 'autoplay=' . $this->autoplay 
      . '&controls=' . $this->controls 
      . '&mute=' . $this->mute 
      . '&enablejsapi=1&loop=' . $this->loop 
      . '&modestbranding=' . $this->modestbranding 
      . '&showinfo=0&autohide=' . $this->autohide 
      . '&rel=0" width="560" frameborder="0" allowfullscreen></iframe>';
  }
}